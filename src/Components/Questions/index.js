import React from "react";
import RadioQuestion from "../Questions/RadioQuestions";
import InputQuestion from "../Questions/InputQuestions";
import MultipleQuestion from "../Questions/MultipleQuestions";

const Questions = ({
  question,
  getRadioAnswer,
  radioChecked,
  inputValue,
  getInputValue,
  formError,
  multipleChecked,
  getMultipleAnswer,
}) => {
  return (
    <>
      <div className="questions-wrapper">
        {question.type === "radio" ? (
          <RadioQuestion {...{ question, getRadioAnswer, radioChecked }} />
        ) : question.type === "input" ? (
          <InputQuestion
            {...{ question, inputValue, getInputValue, formError }}
          />
        ) : question.type === "multiple" ? (
          <MultipleQuestion
            {...{ question, getMultipleAnswer, multipleChecked }}
          />
        ) : null}
      </div>
    </>
  );
};

export default Questions;
